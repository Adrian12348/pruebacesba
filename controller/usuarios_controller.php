<?php

//Tomando el valor de la variable
$action = $_POST["action"];
require_once("../model/model_usuarios.php");
$modelUsuario = new model_usuarios();
//Llamada al modelo
if ($action == "usuariosIndex") {
    $datos = $modelUsuario->usuarios();
} else if ($action == "añadirUsuario") {
    $user = $_POST["user"];
    $pass = $_POST["pass"];
    $rol = $_POST["rol"];
    $response = $modelUsuario->agregarUsuario($user, $pass, $rol);
} else if ($action == "editarUsuario") {
    $user = $_POST["user"];
    $pass = $_POST["pass"];
    $rol = $_POST["rol"];
    $id = $_POST["id"];
    $response = $modelUsuario->editarUsuario($user, $pass, $rol,$id);
}
?>
