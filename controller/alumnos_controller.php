<?php

//Tomando el valor de la variable
$action = $_POST["action"];
require_once("../model/model_alumnos.php");
$modelAlumnos = new model_alumnos();
//Llamada al modelo
if ($action == "alumnosIndex") {
    $datos = $modelAlumnos->alumnos();
} 
else if ($action == "añadirAlumno") {
    $matricula = $_POST["matricula"];
    $nombre = $_POST["nombre"];
    $grado = $_POST["grado"];
    $carrera = $_POST["carrera"];
    $response = $modelAlumnos->agregarAlumno($matricula, $nombre, $grado, $carrera);
} 
else if ($action == "editarAlumno") {
    $matricula = $_POST["matricula"];
    $nombre = $_POST["nombre"];
    $grado = $_POST["grado"];
    $carrera = $_POST["carrera"];
    $response = $modelAlumnos->editarAlumno($matricula, $nombre, $grado, $carrera);
}
?>
