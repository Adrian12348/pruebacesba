<?php

//Tomando el valor de la variable
$action = $_POST["action"];
require_once("../model/model_materias.php");
$modelMaterias = new model_materias();
//Llamada al modelo
if ($action == "materiasIndex") {
    $datos = $modelMaterias->materias();
} 
else if ($action == "añadirMateria") {
    $codigo = $_POST["codigo"];
    $grado = $_POST["grado"];
    $cali = $_POST["cali"];
    $response = $modelMaterias->agregarMateria($codigo, $grado, $cali);
} 
else if ($action == "editarMateria") {
     $codigo = $_POST["codigo"];
    $grado = $_POST["grado"];
    $cali = $_POST["cali"];
    $response = $modelMaterias->editarMateria($codigo, $grado, $cali);
}

?>
