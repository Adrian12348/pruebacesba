<?php

//Tomando el valor de la variable
$action = $_POST["action"];
require_once("../model/model_alumnos.php");
require_once("../model/model_kardex.php");
$modelAlumnos = new model_alumnos();
$modelKardex = new model_kardex();
//Llamada al modelo
if ($action == "kardexIndex") {
    $datos = $modelAlumnos->alumnos();
    
} else if ($action == "kardexRedirect") {
    $matricula = $_POST["matricula"];
    $grado = $_POST["grado"];
    session_start();
    $_SESSION['matricula'] = $matricula;
    $_SESSION['grado'] = $grado;
//    $datos = $modelKardex->alumnos();
    echo "Matricula en sesion ".$_SESSION['matricula'];
    
} else if ($action == "kardexAlumno") {
    $datos = $modelKardex->kardexAlumno();
    
} else if ($action == "codigo") {
    $matricula = $_POST["matricula"];
    $cod = $_POST["cod"];
    $response = $modelKardex->codigo($matricula, $cod);
    
} else if ($action == "añadirKardex") {
    $matricula = $_POST["matricula"];
    $codigo = $_POST["codigo"];
    $cali = $_POST["cali"];
    $response = $modelKardex->agregarKardex($matricula, $codigo, $cali);
    
} else if ($action == "editarKardex") {
    $matricula = $_POST["matricula"];
    $codigo = $_POST["codigo"];
    $cali = $_POST["cali"];
    $id = $_POST["id"];
    $codigoPrev = $_POST["codigoPrev"];
    $response = $modelKardex->editarKardex($matricula, $codigo, $cali, $id, $codigoPrev);
}
?>
