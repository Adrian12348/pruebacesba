<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
        <title></title>
    </head>
    <body >
        <div class="page-container">
            <div class="main-content" style="margin-top: -100px">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30 divTable">
                                    <h3 class="title-3 m-b-30">
                                        <i class="fas fa-users"></i>Alumnos</h3>
                                    <div class="filters m-b-45">
                                        <div class="table-data__tool">
                                            <div class="table-data__tool-right" style="margin-top: 10px">
                                                <button id="openModal" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                    <i class="zmdi zmdi-plus"></i>Agregar alumnos</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-data divTable">
                                        <table class="table" id="tableAlumnos">
                                            <thead>
                                                <tr>
                                                    <th>Matricula</th>
                                                    <th>Nombre</th>
                                                    <th>Grado</th>
                                                    <th>Carrera</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END USER DATA-->
                            </div>
                        </div
                    </div>
                </div>
            </div
        </div>
    </div>
</div>



<!-- Ventana modal -->
<div id="modalAlumno" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1><i class="fas fa-users"></i><label id="titleModal">Alumnos</label></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body card-block">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-hashtag"></i>
                            </div>
                            <input style="display:none" type="email" id="falso" name="falso" placeholder="" class="form-control">
                            <input type="text" id="matricula" name="matricula" placeholder="Matricula" class="form-control">                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-user"></i>
                            </div>
                            <input style="display:none" type="text" />
                            <input type="text" id="nombre" name="nombre" placeholder="Nombre" class="form-control">                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-book"></i>
                            </div>
                            <input type="number" min="1" max="10" id="grado" name="grado" placeholder="Grado" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-graduation-cap"></i>
                            </div>
                            <input style="display:none" type="text" />
                            <input type="text" id="carrera" name="carrera" placeholder="Carrera" class="form-control">                           
                        </div>
                    </div>
                    <div class="form-actions form-group">
                        <button id="guardar" class="btn btn-primary btn-sm">Guardar</button>
                    </div>  
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show"
                         id="alertError" name="alertError"  style="display: none">
                        <span class="badge badge-pill badge-danger">Error</span>
                        Error al ingresar el alumno!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"
                         id="alertSuccess" name="alertSuccess"  style="display: none">
                        <span class="badge badge-pill badge-success">Success</span>
                        Alumno ingresado correctamente!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var idAction = 0;
        var height = $(window).height();
        $('.divTable').height(height);
        var table = $('#tableAlumnos').DataTable({
            ajax: {
                url: "../controller/alumnos_controller.php",
                type: "POST",
                dataSrc: function (data) {
                    console.log(data[0]);
                    // Si hay error y es verdadero o no existe .Mens
                    if (data[0] == "Error") {
                        // Devolver arreglo vacío
                        return [];
                    } else {
                        return data;
                    }
                },
                data:
                        {
                            action: "alumnosIndex",
                        },
            },
            columns: [
                {"data": "matricula"},
                {"data": "nombre"},
                {"data": "grado"},
                {"data": "carrera"}

            ],
            language: {
                "emptyTable": "No hay Alumnos registrados"
            },
        });
        function clearModal() {
            matricula = $("#matricula").val("");
            $("#matricula").prop('disabled', false);
            nombre = $("#nombre").val("");
            grado = $("#grado").val("");
            carrera = $("#carrera").val("");
            idAction = 0;
        }
        function openModal() {
            clearModal()
            titulo = $("#titleModal").text("Agregar alumno");
            $('#modalAlumno').modal('show');
        }

        $('#tableAlumnos tbody').on('dblclick', 'tr', function () {
            var pos = table.row(this).index();
            var row = table.row(pos).data();
            if (row !== undefined) {
                matricula = $("#matricula").val(row.matricula);
                $("#matricula").prop('disabled', true);
                nombre = $("#nombre").val(row.nombre);
                grado = $("#grado").val(row.grado);
                carrera = $("#carrera").val(row.carrera);
                idAction = 1;
                titulo = $("#titleModal").text("Editar alumno");
                $('#modalAlumno').modal('show');
            }
        });

        $("#openModal").click(function () {
            openModal();
        })

        $("#guardar").click(function () {
            var matricula = $("#matricula").val().trim();
            var nombre = $("#nombre").val().trim();
            var grado = $("#grado").val().trim();
            var carrera = $("#carrera").val().trim();
            var action = "";
            if (idAction <= 0) {
                action = "añadirAlumno";
                console.log("insertar", action)
            } else {
                action = "editarAlumno";
                console.log("editar", action)
            }
            if (matricula.length <= 0 || nombre.length <= 0 || grado.length <= 0 || carrera.length <= 0) {
                $("#alertError").show("slow").delay(3000);
                $("#alertError").hide("slow");
                console.log(matricula.length)
            } else {
                console.log(matricula.length)
                $.ajax({
                    url: "../controller/alumnos_controller.php",
                    type: 'POST',
                    data:
                            {
                                matricula: matricula,
                                nombre: nombre,
                                grado: grado,
                                carrera: carrera,
                                action: action,
                            },
                    success: function (response) {
                        if (response == 1) {
                            $("#alertSuccess").show("slow").delay(3000);
                            $("#alertSuccess").hide("slow");
                            setTimeout(function () {
                                location.reload().set;
                            }, 3000);
                        } else {
                            $("#alertError").show("slow").delay(3000);
                            $("#alertError").hide("slow");
                        }
                        console.log(response);
                    }, error: function (response) {
                    }
                })
            }
        })

    });
</script>
</body>
</html>
