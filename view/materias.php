<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
        <style type="text/css">
            .red {
                background-color: red !important;
                color: #ffffff;
            }

        </style>
        <title></title>
    </head>
    <body >
        <div class="page-container">
            <div class="main-content" style="margin-top: -100px">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30 divTable">
                                    <h3 class="title-3 m-b-30">
                                        <i class="fas fa-users"></i>Materias</h3>
                                    <div class="filters m-b-45">
                                        <div class="table-data__tool">
                                            <div class="table-data__tool-right" style="margin-top: 10px">
                                                <button id="openModal" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                    <i class="zmdi zmdi-plus"></i>Agregar materias</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-data divTable">
                                        <table class="table" id="tableMaterias">
                                            <thead>
                                                <tr>
                                                    <th>Código</th>
                                                    <th>Grado</th>
                                                    <th>Calificación min</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END USER DATA-->
                            </div>
                        </div
                    </div>
                </div>
            </div
        </div>
    </div>
</div>



<!-- Ventana modal -->
<div id="modalMateria" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1><i class="fas fa-users"></i><label id="titleModal">Materias</label></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body card-block">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-hashtag"></i>
                            </div>
                            <input style="display:none" type="email" id="falso" name="falso" placeholder="" class="form-control">
                            <input type="text" id="codigo" name="codigo" placeholder="Código" class="form-control">                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-book"></i>
                            </div>
                            <input type="number" min="1" max="10" id="grado" name="grado" placeholder="Grado" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                               <i class="fas fa-sort-numeric-down"></i>
                            </div>
                            <input type="number" min="1" max="10" id="cali" name="cali" placeholder="Calificación minima" class="form-control">
                        </div>
                    </div>
                    <div class="form-actions form-group">
                        <button id="guardar" class="btn btn-primary btn-sm">Guardar</button>
                    </div>  
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show"
                         id="alertError" name="alertError"  style="display: none">
                        <span class="badge badge-pill badge-danger">Error</span>
                        Error al ingresar la materia!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"
                         id="alertSuccess" name="alertSuccess"  style="display: none">
                        <span class="badge badge-pill badge-success">Success</span>
                        Materia ingresada correctamente!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var idAction = 0;
        var height = $(window).height();
        $('.divTable').height(height);
        var table = $('#tableMaterias').DataTable({
            ajax: {
                url: "../controller/materias_controller.php",
                type: "POST",
                dataSrc: function (data) {
                    // Si hay error y es verdadero o no existe .Mens
                    if (data[0] == "Error") {
                        // Devolver arreglo vacío
                        return [];
                    } else {
                        return data;
                    }
                },
                data:
                        {
                            action: "materiasIndex",
                        },
            },
            columns: [
                {"data": "codigo"},
                {"data": "grado"},
                {"data": "calificacion_min"}
            ],
            language: {
                "emptyTable": "No hay Materias registradas"
            },
        });
        function clearModal() {
            codigo = $("#codigo").val("");
            $("#codigo").prop('disabled', false);
            grado = $("#grado").val("");
            cali = $("#cali").val("");
            idAction = 0;
        }
        function openModal() {
            clearModal()
            titulo = $("#titleModal").text("Agregar materia");
            $('#modalMateria').modal('show');
        }

        $('#tableMaterias tbody').on('dblclick', 'tr', function () {
            var pos = table.row(this).index();
            var row = table.row(pos).data();
            if (row !== undefined) {
                codigo = $("#codigo").val(row.codigo);
                $("#codigo").prop('disabled', true);
                grado = $("#grado").val(row.grado);
                cali = $("#cali").val(row.calificacion_min);
                idAction = 1;
                titulo = $("#titleModal").text("Editar materia");
                $('#modalMateria').modal('show');
            }
        });

        $("#openModal").click(function () {
            openModal();
        })

        $("#guardar").click(function () {
            var codigo = $("#codigo").val().trim();
            var grado = $("#grado").val().trim();
            var cali = $("#cali").val().trim();
            var action = "";
            if (idAction <= 0) {
                action = "añadirMateria";
            } else {
                action = "editarMateria";
            }
            if (codigo.length <= 0 || grado.length <= 0 || cali.length <= 0) {
                $("#alertError").show("slow").delay(3000);
                $("#alertError").hide("slow");
            } else {
                $.ajax({
                    url: "../controller/materias_controller.php",
                    type: 'POST',
                    data:
                            {
                                codigo: codigo,
                                grado: grado,
                                cali: cali,
                                action: action,
                            },
                    success: function (response) {
                        if (response == 1) {
                            $("#alertSuccess").show("slow").delay(3000);
                            $("#alertSuccess").hide("slow");
                            setTimeout(function () {
                                location.reload().set;
                            }, 3000);
                        } else {
                            $("#alertError").show("slow").delay(3000);
                            $("#alertError").hide("slow");
                        }
                    }, error: function (response) {
                    }
                })
            }
        })

    });
</script>
</body>
</html>
