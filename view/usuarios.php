<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
        <title></title>
    </head>
    <body >
        <div class="page-container">
            <div class="main-content" style="margin-top: -100px">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30 divTable">
                                    <h3 class="title-3 m-b-30">
                                        <i class="fas fa-users"></i>Usuarios</h3>
                                    <div class="filters m-b-45">
                                        <div class="table-data__tool">
                                            <div class="table-data__tool-right" style="margin-top: 10px">
                                                <button id="openModal" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                    <i class="zmdi zmdi-plus"></i>Agregar usuario</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-data divTable">
                                        <table class="table" id="tableUsuarios">
                                            <thead>
                                                <tr>
                                                    <th>Email</th>
                                                    <th>Rol</th>
                                                    <th>Contraseña</th>
                                                    <th style="display: none"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END USER DATA-->
                            </div>
                        </div
                    </div>
                </div>
            </div
        </div>
    </div>
</div>



<!-- Ventana modal -->
<div id="modalUsuario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1><i class="fas fa-users"></i><label id="titleModal">Usuarios</label></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body card-block">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <input style="display:none" type="email" id="falso" name="falso" placeholder="Usuario" class="form-control">
                            <input type="email" id="user" name="user" placeholder="Usuario" class="form-control">                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-asterisk"></i>
                            </div>
                            <input style="display:none" type="password" />
                            <input type="password" id="pass" name="pass" placeholder="Contraseña" class="form-control">
                            <input style="display:none" type="text" id="idU" name="idU" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Rol</div>
                            <select id="rol" class="form-control">
                                <option value="1">Admin</option>
                                <option value="2">Alumno</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions form-group">
                        <button id="guardar" class="btn btn-primary btn-sm">Guardar</button>
                    </div>  
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show"
                         id="alertError" name="alertError"  style="display: none">
                        <span class="badge badge-pill badge-danger">Error</span>
                        Error al ingresar el usuario!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"
                         id="alertSuccess" name="alertSuccess"  style="display: none">
                        <span class="badge badge-pill badge-success">Success</span>
                        Usuario ingresado correctamente!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var height = $(window).height();

        $('.divTable').height(height);
        var table = $('#tableUsuarios').DataTable({
            ajax: {
                url: "../controller/usuarios_controller.php",
                type: "POST",
                dataSrc: function (data) {
                    // Si hay error y es verdadero o no existe .Mens
                    if (data[0] == "Error") {
                        // Devolver arreglo vacío
                        return [];
                    } else {
                        return data;
                    }
                },
                data:
                        {
                            action: "usuariosIndex",
                        },
            },
            columns: [
                {"data": "email"},
                {"data": "id_rol"},
                {"data": "password"},
                {"data": "id"}
            ],
            language: {
                "emptyTable": "No hay Usuarios registrados"
            },
            createdRow: function (row, data, dataIndex) {
                $($(row).find("td")[3]).hide();
                if (data.id_rol == 1) {
                    table.cell(dataIndex, 1).data("Admin");
                } else {
                    table.cell(dataIndex, 1).data("Alumno");
                }
                console.log(data.id_rol)
            }
        });
        function clearModal() {
            user = $("#user").val("");
            pass = $("#pass").val("");
            id = $("#idU").val("");
        }
        function openModal() {
            clearModal()
            titulo = $("#titleModal").text("Agregar usuario");
            $('#modalUsuario').modal('show');
        }

        $('#tableUsuarios tbody').on('dblclick', 'tr', function () {
            var pos = table.row(this).index();
            var row = table.row(pos).data();
            if (row !== undefined) {
                console.log(row.email);
                user = $("#user").val(row.email);
                pass = $("#pass").val(row.password);
                id = $("#idU").val(row.id);
                if (row.id_rol == "Admin") {
                    rol = $("#rol").val(1);
                } else {
                    rol = $("#rol").val(2);
                }
                titulo = $("#titleModal").text("Editar usuario");
                $('#modalUsuario').modal('show');
            }
        });

        $("#openModal").click(function () {
            openModal();
        })

        $("#guardar").click(function () {
            var user = $("#user").val();
            var pass = $("#pass").val();
            var rol = $("#rol").val();
            var id = $("#idU").val();
            var action = "";
            if (id.length <= 0) {
                action = "añadirUsuario";
                console.log("insertar", id)
            } else {
                action = "editarUsuario";
                console.log("editar", id)
            }
            var noValido = /\s/;
            if (user.length <= 0 || pass.length <= 0 || noValido.test(pass) || rol.length <= 0) {
                $("#alertError").show("slow").delay(3000);
                $("#alertError").hide("slow");
            } else {
                $.ajax({
                    url: "../controller/usuarios_controller.php",
                    type: 'POST',
                    data:
                            {
                                user: user,
                                pass: pass,
                                rol: rol,
                                id: id,
                                action: action,
                            },
                    success: function (response) {
                        if (response == 1) {
                            $("#alertSuccess").show("slow").delay(3000);
                            $("#alertSuccess").hide("slow");
                            setTimeout(function () {
                                location.reload().set;
                            }, 3000);
                        } else {
                            $("#alertError").show("slow").delay(3000);
                            $("#alertError").hide("slow");
                        }
                        console.log(response);
                    }, error: function (response) {
                    }
                })
            }
        })

    });
</script>
</body>
</html>
