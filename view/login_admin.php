<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <!-- Title Page-->
        <title>Login</title>

        <!-- Fontfaces CSS-->
        <link href="./view/css/font-face.css" rel="stylesheet" media="all">
        <link href="./view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="./view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="./view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="./view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="./view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="./view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="./view/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="./view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="./view/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="./view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="./view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="css/theme.css" rel="stylesheet" media="all">

    </head>

    <body class="animsition">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <h1>Login Admin</h1>
                        </div>
                        <div class="login-form">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="au-input au-input--full" type="email" id="user" name="user" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input class="au-input au-input--full" type="password" id="pass" name="pass" placeholder="Password">
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" id="login" name="login">Ingresar</button>
                            <div class="register-link">
                                <p>
                                    ¿Eres alumno?
                                    <a href="./view/login_alumno_index.php">Ingresa Aquí</a>
                                </p>
                            </div>
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show"
                                 id="alert" name="alert" style="display: none" >
                                <span class="badge badge-pill badge-danger">Error</span>
                                Usuario o contraseña no valido!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Jquery JS-->
        <script src="./view/vendor/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap JS-->
        <script src="./view/vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="./view/vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="./view/vendor/slick/slick.min.js">
        </script>
        <script src="./view/vendor/wow/wow.min.js"></script>
        <script src="./view/vendor/animsition/animsition.min.js"></script>
        <script src="./view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="./view/vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="./view/vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="./view/vendor/circle-progress/circle-progress.min.js"></script>
        <script src="./view/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="./view/vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="./view/vendor/select2/select2.min.js">
        </script>

        <!-- Main JS-->
        <script src="./view/js/main.js"></script>

        <!-- Script de validación -->
        <script src="./view/js/jquery-3.2.1.min.js"></script>
        <script src="./view/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                $("#login").click(function () {
                    var user = $("#user").val();
                    var pass = $("#pass").val();
                    console.log(user);
                    $.ajax({
                        url: "./controller/login_controller.php",
                        type: 'POST',
                        data:
                                {
                                    user: user,
                                    pass: pass,
                                    action: "admin",
                                },
                        success: function (response) {
                            var userInf = JSON.parse(response);
                            console.log(userInf[0].id_rol);
                            if (userInf[0].id_rol == 1) {
                                $(location).attr('href', './view/principal_index.php');
                            } else {
                                $("#alert").show("slow").delay(3000);
                                $("#alert").hide("slow");
                            }
                        }, error: function (response) {
                        }
                    })
                })
            });
        </script>

    </body>

</html>
<!-- end document-->