<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <!-- Title Page-->
        <title>CESBA</title>

        <!-- Fontfaces CSS-->
        <link href="css/font-face.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="css/theme.css" rel="stylesheet" media="all">
        <?php
        session_start();
        $usuario = $_SESSION['usuario'];
        $rol = $_SESSION['rol'];
        ?>
    </head>

    <body class="animsition">

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="../view/principal_index.php">
                    <img src="cesba.png" alt="CoolAdmin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php
                        if ($rol == 1) {
                            ?>

                            <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-users"></i>Usuarios</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <!--                                <li>
                                                                        <a href="#"><i class="fas fa-user-plus"></i>Agregar usuario</a>
                                                                    </li>-->
                                    <li>
                                        <a id="editarUsuario" href="#"><i class="fas fa-edit"></i>Editar usuario</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-graduation-cap"></i>Alumnos</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <!--                                <li>
                                                                        <a href="index1.html"><i class="fas fa-user-plus"></i>Agregar alumno</a>
                                                                    </li>-->
                                    <li>
                                        <a id="editarAlumno" href="#"><i class="fas fa-edit"></i>Editar alumno</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-book"></i>Materias</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <!--                                <li>
                                                                        <a href="index1.html"><i class="fas fa-plus-circle"></i>Agregar materia</a>
                                                                    </li>-->
                                    <li>
                                        <a id="editarMateria" href="#"><i class="fas fa-edit"></i>Editar materia</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-sort-numeric-down"></i>Kardex</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a id="editarKardex" href="#"><i class="fas fa-edit"></i>Asignar Kardex</a>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>
                                <?php
                                echo $usuario;
                                ?>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <?php
                                    if ($rol == 2) {
                                        ?>
                                        <a id="prueba" href="#"><i class="fas fa-edit"></i>Consultar Kardex</a>
                                        <?php
                                    }
                                    ?>
                                    <a id="cerrarSesion" href="../index.php"><i class="fas fa-edit"></i>Cerrar sesion</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- Jquery JS-->
        <script src="vendor/jquery-3.5.1.js"></script>
        <!-- Bootstrap JS-->
        <script src="vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="vendor/slick/slick.min.js">
        </script>
        <script src="vendor/wow/wow.min.js"></script>
        <script src="vendor/animsition/animsition.min.js"></script>
        <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="vendor/circle-progress/circle-progress.min.js"></script>
        <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="vendor/select2/select2.min.js">
        </script>

        <!-- Main JS-->
        <script src="js/main.js"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                $("#editarUsuario").click(function () {
                    $.ajax({
                        url: "../controller/usuarios_controller.php",
                        type: 'POST',
                        data:
                                {
                                    action: "usuariosIndex",
                                },
                        success: function (response) {
                            $(location).attr('href', '../view/usuarios_index.php');
                        }, error: function (response) {
                        }
                    })

                })
                $("#editarAlumno").click(function () {
                    $.ajax({
                        url: "../controller/alumnos_controller.php",
                        type: 'POST',
                        data:
                                {
                                    action: "alumnosIndex",
                                },
                        success: function (response) {
                            $(location).attr('href', '../view/alumnos_index.php');
                        }, error: function (response) {
                        }
                    })
                })
                $("#editarMateria").click(function () {
                    $.ajax({
                        url: "../controller/materias_controller.php",
                        type: 'POST',
                        data:
                                {
                                    action: "materiasIndex",
                                },
                        success: function (response) {
                            $(location).attr('href', '../view/materias_index.php');
                        }, error: function (response) {
                        }
                    })
                })
                $("#editarKardex").click(function () {
                    $.ajax({
                        url: "../controller/kardex_controller.php",
                        type: 'POST',
                        data:
                                {
                                    action: "kardexIndex",
                                },
                        success: function (response) {
                            $(location).attr('href', '../view/kardex_index.php');
                        }, error: function (response) {
                        }
                    })
                })
                $("#prueba").click(function () {
                    $.ajax({
                        url: "../controller/consulta_kardex_controller.php",
                        type: 'POST',
                        data:
                                {
                                    action: "kardexIndex",
                                },
                        success: function (response) {
                            $(location).attr('href', '../view/consulta_kardex_index.php');
                        }, error: function (response) {
                        }
                    })
                })
            });
        </script>
    </body>

</html>
<!-- end document-->
