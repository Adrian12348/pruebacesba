<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
        <title></title>
    </head>
    <body >
        <div class="page-container">
            <div class="main-content" style="margin-top: -100px">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30 divTable">
                                    <h3 class="title-3 m-b-30">
                                        <i class="fas fa-users"></i>Kardex</h3>
                                    <div class="table-responsive table-data divTable">
                                        <table class="table" id="tableKardex">
                                            <thead>
                                                <tr>
                                                    <th>Matricula</th>
                                                    <th>Nombre</th>
                                                    <th>Grado</th>
                                                    <th>Carrera</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END USER DATA-->
                            </div>
                        </div
                    </div>
                </div>
            </div
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalKardex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar kardex</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label id="pregunta"></label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="asignar" type="button" class="btn btn-primary">Asignar materias</button>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var matricula;
        var grado;
        var height = $(window).height();
        $('.divTable').height(height);
        var table = $('#tableKardex').DataTable({
            ajax: {
                url: "../controller/kardex_controller.php",
                type: "POST",
                dataSrc: function (data) {
                    console.log(data[0]);
                    // Si hay error y es verdadero o no existe .Mens
                    if (data[0] == "Error") {
                        // Devolver arreglo vacío
                        return [];
                    } else {
                        return data;
                    }
                },
                data:
                        {
                            action: "kardexIndex",
                        },
            },
            columns: [
                {"data": "matricula"},
                {"data": "nombre"},
                {"data": "grado"},
                {"data": "carrera"}

            ],
            language: {
                "emptyTable": "No hay Alumnos registrados"
            },
        });

        $('#tableKardex tbody').on('dblclick', 'tr', function () {
            var pos = table.row(this).index();
            var row = table.row(pos).data();
            if (row !== undefined) {
                pregunta = $("#pregunta").text("¿Seguro que quiere asignar materias para el alumno '" + row.nombre 
                        + "' con matricula: '" + row.matricula +"'?");
                matricula = row.matricula;
                grado = row.grado;
                $('#modalKardex').modal('show');
            }
        });

        $("#asignar").click(function () {
            console.log(matricula)
            $.ajax({
                url: "../controller/kardex_controller.php",
                type: 'POST',
                data:
                        {
                            matricula: matricula,
                            grado: grado,
                            action: "kardexRedirect",
                        },
                success: function (response) {
                    $(location).attr('href', '../view/kardex_alumno_index.php');
                }, error: function (response) {
                }
            })

        })

    });
</script>
</body>
</html>
