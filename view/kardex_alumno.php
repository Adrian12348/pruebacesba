<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
        <title></title>
        <?php
        session_start();
        $matricula = $_SESSION['matricula'];
        ?>
    </head>
    <body >
        <div class="page-container">
            <div class="main-content" style="margin-top: -100px">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30 divTable">
                                    <h3 class="title-3 m-b-30">
                                        <i class="fas fa-users"></i>Kardex alumno:
                                        <label id="matriculaLabel">
                                            <?php
                                            echo $matricula;
                                            ?>
                                        </label>  
                                    </h3>
                                    <div class="filters m-b-45">
                                        <div class="table-data__tool">
                                            <div class="table-data__tool-right" style="margin-top: 10px">
                                                <button id="openModal" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                    <i class="zmdi zmdi-plus"></i>Asignar materia</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-data divTable">
                                        <table class="table" id="tableKardex">
                                            <thead>
                                                <tr>
                                                    <th>Matricula</th>
                                                    <th>Codigo de materia</th>
                                                    <th>Calificación</th>
                                                    <th style="display: none"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END USER DATA-->
                            </div>
                        </div
                    </div>
                </div>
            </div
        </div>
    </div>
</div>

<!-- Ventana modal -->
<div id="modalKardex" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1><i class="fas fa-users"></i><label id="titleModal">Asignar Materias</label></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body card-block">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-hashtag"></i>
                            </div>
                            <input style="display:none" type="email" id="falso" name="falso" placeholder="" class="form-control">
                            <input type="text" id="matricula" name="matricula" placeholder="Matricula" class="form-control">                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-book"></i>
                            </div>     
                            <select id="codigo" class="form-control">
                            </select>
                            <input style="display:none" type="text" id="idK" name="idK" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fas fa-sort-numeric-down"></i>
                            </div>
                            <input type="number" min="1" max="10" id="cali" name="cali" placeholder="Calificación" class="form-control">
                        </div>
                    </div>
                    <div class="form-actions form-group">
                        <button id="guardar" class="btn btn-primary btn-sm">Guardar</button>
                    </div>  
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show"
                         id="alertError" name="alertError"  style="display: none">
                        <span class="badge badge-pill badge-danger">Error</span>
                        Error al asignar la materia!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"
                         id="alertSuccess" name="alertSuccess"  style="display: none">
                        <span class="badge badge-pill badge-success">Success</span>
                        Materia asignada correctamente!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var matricula = $("#matriculaLabel").text().trim();
        var height = $(window).height();
        var codigoPrev;
        $('.divTable').height(height);
        var table = $('#tableKardex').DataTable({
            ajax: {
                url: "../controller/kardex_controller.php",
                type: "POST",
                dataSrc: function (data) {
//                    matricula = data[0].matricula_alumno;
                    // Si hay error y es verdadero o no existe .Mens
                    if (data[0] == "Error") {
                        // Devolver arreglo vacío
                        return [];
                    } else {
                        return data;
                    }
                },
                data:
                        {
                            action: "kardexAlumno",
                        },
            },
            columns: [
                {"data": "matricula_alumno"},
                {"data": "codigo_materia"},
                {"data": "calificacion"},
                {"data": "id"}

            ],
            language: {
                "emptyTable": "No hay materias asignadas"
            },
            createdRow: function (row, data, dataIndex) {
                $($(row).find("td")[3]).hide();
            },
        });
        function clearModal() {
            $("#matricula").prop('disabled', true);
            $("#matricula").val(matricula);
            codigo = $("#codigo").val("");
            cali = $("#cali").val("");
            id = $("#idK").val("");
        }

        function cargarOptions( cod) {
            console.log(cod)
            $.ajax({
                url: "../controller/kardex_controller.php",
                dataType: 'html',
                type: 'POST',
                data:
                        {
                            matricula: matricula,
                            action: "codigo",
                            cod : cod
                        },
                success: function (response) {
                    console.log(response);
                    codigo.html(response);
                }, error: function (response) {
                }
            })
        }

        function openModal() {
            clearModal()
            titulo = $("#titleModal").text("Asignar materia nueva");
            cargarOptions("insertar");
            $('#modalKardex').modal('show');
        }

        $("#openModal").click(function () {
            console.log(matricula)
            openModal();
        })

        $('#tableKardex tbody').on('dblclick', 'tr', function () {
            var pos = table.row(this).index();
            var row = table.row(pos).data();
            if (row !== undefined) {
                clearModal();
                cargarOptions("editar");
                setTimeout(function () {
                    codigoPrev = row.codigo_materia;
                    $("#codigo").val(row.codigo_materia);
                    $("#cali").val(row.calificacion);
                    $("#idK").val(row.id);
                    titulo = $("#titleModal").text("Editar asignación de materia");
                    console.log($("#idK").val());
                    $('#modalKardex').modal('show');
                }, 200);
            }
        });
        $("#guardar").click(function () {
            var codigo = $("#codigo").val().trim();
            var cali = $("#cali").val().trim();
            var id = $("#idK").val();
            var action = "";
            if (id.length <= 0) {
                action = "añadirKardex";
                console.log("insertar", id)
            } else {
                action = "editarKardex";
                console.log("editar", id)
            }
            if (codigo.length <= 0 || cali.length <= 0) {
                $("#alertError").show("slow").delay(3000);
                $("#alertError").hide("slow");
            } else {
                $.ajax({
                    url: "../controller/kardex_controller.php",
                    type: 'POST',
                    data:
                            {
                                matricula: matricula,
                                codigo: codigo,
                                cali: cali,
                                id : id,
                                action: action,
                                codigoPrev: codigoPrev,
                            },
                    success: function (response) {
                        if (response == 1) {
                            $("#alertSuccess").show("slow").delay(3000);
                            $("#alertSuccess").hide("slow");
                            setTimeout(function () {
                                location.reload().set;
                            }, 3000);
                        } else {
                            console.log(response);
                            $("#alertError").show("slow").delay(3000);
                            $("#alertError").hide("slow");
                        }
                    }, error: function (response) {
                    }
                })
            }
        })
    });
</script>
</body>
</html>
