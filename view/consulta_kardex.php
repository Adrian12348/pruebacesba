<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
        <style type="text/css">
            .red {
                background-color: red !important;
                color: #ffffff;
            }

        </style>
        <title></title>
        <?php
        session_start();
        $matricula = $_SESSION['matricula'];
        ?>
    </head>
    <body >
        <div class="page-container">
            <div class="main-content" style="margin-top: -100px">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- USER DATA-->
                                <div class="user-data m-b-30 divTable">
                                    <h3 class="title-3 m-b-30">
                                        <i class="fas fa-users"></i>Kardex alumno:
                                        <label id="matriculaLabel">
                                            <?php
                                            echo $matricula;
                                            ?>
                                        </label>  
                                    </h3>
                                    <div class="table-responsive table-data divTable">
                                        <table class="table" id="tableKardex">
                                            <thead>
                                                <tr>
                                                    <th>Matricula</th>
                                                    <th>Codigo de materia</th>
                                                    <th>Calificación</th>
                                                    <th style="display: none"></th>
                                                    <th style="display: none"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END USER DATA-->
                            </div>
                        </div
                    </div>
                </div>
            </div
        </div>
    </div>
</div>

<script src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var height = $(window).height();
        $('.divTable').height(height);
        var table = $('#tableKardex').DataTable({
            ajax: {
                url: "../controller/consulta_kardex_controller.php",
                type: "POST",
                dataSrc: function (data) {
//                    matricula = data[0].matricula_alumno;
                    // Si hay error y es verdadero o no existe .Mens
                    if (data[0] == "Error") {
                        // Devolver arreglo vacío
                        return [];
                    } else {
                        return data;
                    }
                },
                data:
                        {
                            action: "kardexAlumno",
                        },
            },
            columns: [
                {"data": "matricula_alumno"},
                {"data": "codigo_materia"},
                {"data": "calificacion"},
                {"data": "id"},
                {"data": "calificacion_min"},

            ],
            language: {
                "emptyTable": "No hay materias asignadas"
            },
            createdRow: function (row, data, dataIndex) {
                 console.log(data.calificacion, data.calificacion_min, data.calificacion < data.calificacion_min);
                if (parseInt(data.calificacion) < parseInt(data.calificacion_min)) {
                    $($(row).find("td")[2]).addClass('red');
                }
                $($(row).find("td")[3]).hide();
                $($(row).find("td")[4]).hide();
            },
        });
    });
</script>
</body>
</html>
