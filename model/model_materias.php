<?php

include_once '../db/db.php';

class model_materias {

    private $db;
    private $materia;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->materia = array();
    }

    public function materias() {
        $query = "SELECT * FROM materia";
        $result = $this->db->query($query);
        $rows = mysqli_num_rows($result);
        if ($rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $this->materia[] = $rows;
            }
        } else {
            $this->materia[] = "Error";
        }
        echo json_encode($this->materia, TRUE);
    }

    public function agregarMateria($codigo, $grado, $cali) {
        $query = "INSERT INTO materia (codigo, grado, calificacion_min) VALUES('$codigo', '$grado', '$cali')";
        if (!$this->db->query($query)) {
            die('Error: Query no ejecutada' . mysqli_error());
        } else {
            echo 1;
        }
    }
    public function editarMateria($codigo, $grado, $cali) {
        $query = "UPDATE materia SET grado='$grado', calificacion_min='$cali' WHERE codigo='$codigo'";
        if (!$this->db->query($query)) {
            die('Error: Query no ejecutada' . mysqli_error());
        } else {
            echo 1;
        }
    }

}
?>

