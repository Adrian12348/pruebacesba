<?php

include_once '../db/db.php';

class model_alumnos {

    private $db;
    private $alumno;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->alumno = array();
    }

    public function alumnos() {
        $query = "SELECT * FROM alumno";
        $result = $this->db->query($query);
        $rows = mysqli_num_rows($result);
        if ($rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $this->alumno[] = $rows;
            }
        } else {
            $this->alumno[] = "Error";
        }
        echo json_encode($this->alumno, TRUE);
    }

    public function agregarAlumno($matricula, $nombre, $grado, $carrera) {
        $query = "INSERT INTO alumno (matricula, nombre, grado, carrera) VALUES('$matricula', '$nombre', $grado, '$carrera')";
        if (!$this->db->query($query)) {
            die('Error: Query no ejecutada' . mysqli_error());
        } else {
            echo 1;
        }
    }
    public function editarAlumno($matricula, $nombre, $grado, $carrera) {
        $query = "UPDATE alumno SET nombre='$nombre', grado='$grado', carrera='$carrera' WHERE matricula='$matricula'";
        if (!$this->db->query($query)) {
            die('Error: Query no ejecutada' . mysqli_error());
        } else {
            echo 1;
        }
    }

}
?>

