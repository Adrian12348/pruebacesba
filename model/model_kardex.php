<?php
include_once '../db/db.php';

class model_kardex {

    private $db;
    private $kardex;
    private $materias;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->alumno = array();
        $this->materias = array();
        $this->kardex = array();
        session_start();
    }

    public function kardexAlumno() {
        $matricula = $_SESSION['matricula'];
        $query = "SELECT * FROM kardex WHERE matricula_alumno = $matricula";
        $result = $this->db->query($query);
        $rows = mysqli_num_rows($result);
        if ($rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $this->alumno[] = $rows;
            }
        } else {
            $this->alumno[] = "Error";
        }
        echo json_encode($this->alumno, TRUE);
    }

    public function codigo($matricula, $cod) {
        $grado = $_SESSION['grado'];
        $queryMateria = "SELECT * FROM materia WHERE grado = $grado";
        $resultMateria = $this->db->query($queryMateria);
        $rowsMateria = mysqli_num_rows($resultMateria);
        if ($rowsMateria > 0) {
            while ($rowsMateria = $resultMateria->fetch_assoc()) {
                $this->materias[] = $rowsMateria;
            }
        }
        ?>
        <option value="">- Seleccione -</option>
        <?php
        foreach ($this->materias as $materia) {
            $codigo = $materia['codigo'];
            $query = "SELECT * FROM kardex WHERE matricula_alumno = $matricula AND codigo_materia = '$codigo'";
            $result = $this->db->query($query);
            $rows = mysqli_num_rows($result);
            if ($rows > 0) {
                if ($cod == "insertar") {
                    ?>
                    <option disabled value="<?= $materia['codigo'] ?>" ><?= $materia['codigo'] ?></option>
                    <?php
                } else {
                    ?>
                    <option value="<?= $materia['codigo'] ?>"><?= $materia['codigo'] ?></option>
                    <?php
                }
            } else {
                ?>
                <option value="<?= $materia['codigo'] ?>"><?= $materia['codigo'] ?></option>
                <?php
            }
        }
    }

    public function agregarKardex($matricula, $codigo, $cali) {
        $query = "INSERT INTO kardex (matricula_alumno, codigo_materia, calificacion) VALUES('$matricula', '$codigo', $cali)";
        if (!$this->db->query($query)) {
            die('Error: Query no ejecutada' . mysqli_error());
        } else {
            echo 1;
        }
    }

    public function editarKardex($matricula, $codigo, $cali, $id, $codigoPrev) {
        $query = "SELECT * FROM kardex WHERE matricula_alumno = $matricula AND codigo_materia = '$codigo'";
        $result = $this->db->query($query);
        $rows = mysqli_num_rows($result);
        if ($rows > 0) {
            if ($codigoPrev == $codigo) {
                $query = "UPDATE kardex SET codigo_materia='$codigo', calificacion='$cali' WHERE id='$id'";
                if (!$this->db->query($query)) {
                    die('Error: Query no ejecutada' . mysqli_error());
                } else {
                    echo 1;
                }
            } else {
                echo 0;
            }
        } else {
            $query = "UPDATE kardex SET codigo_materia='$codigo', calificacion='$cali' WHERE id='$id'";
            if (!$this->db->query($query)) {
                die('Error: Query no ejecutada' . mysqli_error());
            } else {
                echo 1;
            }
        }
    }

}
?>

