<?php

include_once '../db/db.php';

class model_consulta_kardex {

    private $db;
    private $kardex;
    private $materias;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->alumno = array();
        $this->materias = array();
        $this->kardex = array();
        session_start();
    }

    public function kardexAlumno() {
        $matricula = $_SESSION['matricula'];
        $query = "SELECT K.id, k.codigo_materia,K.matricula_alumno, K.calificacion, M.calificacion_min 
            FROM kardex k, materia M 
            WHERE (K.codigo_materia = M.codigo) 
            AND (K.matricula_alumno = '$matricula')";
        $result = $this->db->query($query);
        $rows = mysqli_num_rows($result);
        if ($rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $this->alumno[] = $rows;
            }
        } else {
            $this->alumno[] = "Error";
        }
        echo json_encode($this->alumno, TRUE);
    }

}
?>

